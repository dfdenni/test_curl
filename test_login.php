<?php 

define('PATH_CONFIG', 'app/config/');
define('PATH_CONTROLLER', 'app/controller/');
define('PATH_MODEL', 'app/model/');
define('PATH_LIBRARY', 'app/libraries/');
define('PATH_VENDOR', 'vendor/');

require_once (PATH_CONFIG . 'definition.php');
require_once (PATH_LIBRARY . 'curl-init/HmacIshin/MyHmacIshin.php');
require_once (PATH_LIBRARY . 'curl-init/HmacIshin/MyTokenHmacIshin.php');
require_once (PATH_LIBRARY . 'curl-init/HmacIshin/MyTransactionHmacIshin.php');

// Bearer using JWT
function __getLoginToken($url, $key_auth, $data_request) {
    return \MyTokenHmacIshin::createHmacToken($url, $key_auth, $data_request);
}

$key_auth = null;
$data_request_login = array(
    'user'      => CLIENT_ID, 
    'password'  => CLIENT_SECRET
);

$basic_key = base64_encode(CLIENT_ID.":".CLIENT_SECRET);
echo "\n";
echo "Login - Result(s): \n";
echo "------------------------------\n";
echo ">> Login User\n";
print_r($data_request_login);
echo "\n";
echo "Basic " . $basic_key . "\n";
echo "\n\n";

echo ">> Hmac 256 - Access Token (Bearer)\n";
$bearer_key_auth  = __getLoginToken(ISHIN_SIGN_IN, $key_auth, $data_request_login);
print_r($bearer_key_auth);
echo "\n\n";
